﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Web_1.Models
{
    public class Response
    {
        [Required(ErrorMessage = "Пожалуйста, введите свое имя")]
        public string Name { get; set; }
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Вы ввели некорректный email")]
        public string Email { get; set; }
  
        [Required(ErrorMessage = "Пожалуйста, укажите, примите ли участие в вечеринке")]
        public bool? WillAttend { get; set; }
    
        private MailAddress from;
        private MailAddress to;
        private MailMessage message;
        private SmtpClient smtpClient;
       
        public void Send(string _from, string _body)
        {    from = new MailAddress("ya.ne.booka@gmail.com");

            smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                Credentials = new NetworkCredential(from.Address, "1234567bg"),
                EnableSsl = true
            };
            to = new MailAddress("ya.ne.booka@gmail.com");
            message = new MailMessage(from, to)
            {
                Subject = "Answer of " + _from,
                Body = _body
            };
            smtpClient.Send(message);
        }
    }
}